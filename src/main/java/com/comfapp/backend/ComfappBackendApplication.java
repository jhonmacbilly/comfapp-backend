package com.comfapp.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComfappBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComfappBackendApplication.class, args);
	}

}
