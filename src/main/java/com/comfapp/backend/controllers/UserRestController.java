package com.comfapp.backend.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.comfapp.backend.models.entities.User;
import com.comfapp.backend.models.services.IUserService;

@CrossOrigin(origins = {"*"})
@RestController
public class UserRestController {
    
    private final IUserService userService;

    private final Logger log = LoggerFactory.getLogger(UserRestController.class);

    UserRestController(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public List<User> index (){
        return userService.findAll();
    }

    @GetMapping("/users/{id}")
	public ResponseEntity<?> show(@PathVariable String id) {
        User user = null;
		Map<String, Object> response = new HashMap<>();
		try {
			user = userService.findByIdentification(id);
		} catch (DataAccessException e) {
            response.put("estado", "Fail");
			response.put("mensaje", "Error al realizar la consulta en la base de datos!");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if(user == null) {
            response.put("estado", "Fail");
			response.put("mensaje", "El usuario ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
        response.put("estado", "Successful");
        response.put("data", user);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
    
}
