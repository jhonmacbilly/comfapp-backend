package com.comfapp.backend.models.entities;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.PostLoad;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "nomin150")
public class User {
    @Id
    String docemp;

    @Column()
	Date fecexp;
    
    @Column()
	Date fecnac;

    @Column()
	String ciuexp;

    @Column()
	String coddoc;
    
    @Column(length = 45)
	String prinom;

    @Column(length = 45)
	String segnom;

    @Column(length = 45)
	String priape;

    @Column(length = 45)
	String segape;

	@Column()
	String estado;

    @Column(length = 1)
	String sexo;
	
	@Column(nullable = true, length = 90)
	String email;

	@Column(nullable = true, length = 3)
	String grusan;

    @Column()
	Integer estrato;

    @Column()
	Integer numhij;
	
	@Column()
	String coddep;

	@Column(name = "detalle")
	String nombreDependencia;

	@PostLoad
	protected void trim () {
		if(this.docemp!=null){
			this.docemp=this.docemp.trim();
		}
		if(this.prinom!=null){
			this.prinom=this.prinom.trim();
		}
		if(this.segnom!=null){
			this.segnom=this.segnom.trim();
		}
		if(this.priape!=null){
			this.priape=this.priape.trim();
		}
		if(this.segape!=null){
			this.segape=this.segape.trim();
		}
		if(this.email!=null){
			this.email=this.email.trim();
		}
		if(this.nombreDependencia!=null){
			this.nombreDependencia=this.nombreDependencia.trim();
		}
	}
}
