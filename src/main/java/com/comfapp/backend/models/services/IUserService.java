package com.comfapp.backend.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.comfapp.backend.models.entities.User;

public interface IUserService {
    public List<User> findAll();
    public Page<User> findAll(Pageable page);
    public User findByIdentification(String id);
    public User save(User user);
}
