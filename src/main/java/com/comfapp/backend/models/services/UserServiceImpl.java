package com.comfapp.backend.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.comfapp.backend.models.dao.IUserDao;
import com.comfapp.backend.models.entities.User;

@Service
public class UserServiceImpl implements IUserService{

    @Autowired
    private IUserDao userDao;

    @Override
    public List<User> findAll() {
        return (List<User>) userDao.findAll();
    }

    @Override
    public Page<User> findAll(Pageable page) {
        return userDao.findAll(page);
    }

    @Override
    public User findByIdentification(String id) {
        return userDao.findByIdentification(id);
    }

    @Override
    public User save(User user) {
        return userDao.save(user);
    }
    
}
