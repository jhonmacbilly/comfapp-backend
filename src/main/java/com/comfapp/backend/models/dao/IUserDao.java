package com.comfapp.backend.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.comfapp.backend.models.entities.User;

public interface IUserDao extends JpaRepository<User, String> {
     @Query(
        value = "SELECT n.docemp, n.fecexp, n.fecnac, n.ciuexp, n.coddoc, n.prinom, n.segnom, n.priape, n.segape, n.sexo, n.email, n.grusan, n.estrato, n.numhij, n1.estado, n1.coddep, n2.detalle " + 
                "FROM nomin150 n, nomin151 n1, nomin09 n2 " + 
                "WHERE n.docemp = n1.docemp " + 
                "AND n1.estado = 'A' " + 
                "AND n.docemp = ?1 " +
                "AND n1.coddep = n2.coddep;", 
        nativeQuery = true)
    public User findByIdentification( String id);
}
